﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArvudMassiivis
{
    class Program
    {
        static int[,] Arvud = new int[10, 10];
        static Random r = new Random(10);

        static void Main(string[] args)
        {
            Algväärtused();

            for (int i = 0; i < Arvud.GetLength(0); i++)
            {
                Console.Write($"\nRida {i+1}: ");
                for (int j = 0; j < Arvud.GetLength(1); j++)
                {
                    Console.Write($"{Arvud[i, j]:00} ");
                }
                
            }
            Console.WriteLine();

 

        }

        static void Algväärtused()
        {
            for (int i = 0; i < Arvud.GetLength(0); i++)
                for (int j = 0; j < Arvud.GetLength(1); j++)
                {
                    Arvud[i, j] = r.Next() % 100;
                }
 

        }
    }
}
