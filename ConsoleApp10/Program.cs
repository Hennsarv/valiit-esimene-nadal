﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 77;
            double e = 77.7654321;

            Console.WriteLine("i:000 {0} {0:000}", i);
            Console.WriteLine("e:F2 {0} {0:F2}", e);
            Console.WriteLine("e:F0 {0} {0:F0}", e);
            Console.WriteLine();
            Console.WriteLine($"i:000 {i} {i:000}");
            Console.WriteLine($"e:F2 {e} {e:F2}");
            Console.WriteLine($"e:F0 {e} {e:F0}");
            Console.WriteLine();
            Console.WriteLine($"Raha {0}", 
                123.456.ToString("C2", CultureInfo.CreateSpecificCulture("da-DK")));

        }
    }
}
