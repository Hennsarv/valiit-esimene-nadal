﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{

    
    class Program
    {


        static void Main(string[] args)
        {
            // mis asi ja miks on massiv

            int[] arvud;
            string[] nimed = { "Henn", "Ants", "Peeter" };

            nimed[1] = "Joosep";

            int[][] tabel1 =
                {
                    new int[2] { 1, 2 },
                    new int[3] { 1, 2, 3 },
                    new int[4]{ 1, 2, 3, 4 }
                };

            int[,] tabel2 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };

            foreach (int[] x in tabel1)
                foreach (int x1 in x)
                    Console.WriteLine(x1);

            foreach (int x in tabel2) Console.WriteLine(x);

            for (int i = 0; i < tabel1.Length; i++)
            {
                for (int j = 0; j < tabel1[i].Length; j++)
                {
                    Console.WriteLine($"T1 rida {i} veerg {j} asub {tabel1[i][j]}");
                }
            }

            for (int i = 0; i < tabel2.GetLength(0); i++)
            {
                for (int j = 0; j < tabel2.GetLength(1); j++)
                {
                    Console.WriteLine($"T2 rida {i} veerg {j} asub {tabel2[i, j]}");

                }
            }

            
            arvud = new int[10];

            int[] teisedarvud = arvud;
            arvud[4] = 77;
            Console.WriteLine(teisedarvud[4]);

            List<List<int>> lii = new List<List<int>>();

            Dictionary<String, Dictionary<string, string>> nimekiri 
                = new Dictionary<string, Dictionary<string, string>>();

            nimekiri.Add("1a", new Dictionary<string, string>());
            nimekiri.Add("2b", new Dictionary<string, string>());

            nimekiri["1a"].Add("Henn", "5");
            nimekiri["1a"].Add("Ants", "2");
            nimekiri["1a"].Add("Peeter", "3");

            nimekiri["2b"].Add("Pille", "Suurepärane");
            nimekiri["2b"].Add("Malle", "4");

            int v;
            if (int.TryParse(nimekiri["1a"]["Ants"], out v))
                nimekiri["1a"]["Ants"] = (v++).ToString();
                    ;

            Console.WriteLine(nimekiri["1a"]["Ants"]);

            // kelle jäi segaseks ? : avaldis

            foreach (var x in nimekiri)
            {
                Console.WriteLine($"klass {x.Key} õpilased:");
                foreach (var y in x.Value)

                {
                    Console.WriteLine(
                        "õpilane {0} hinne {1} on {2}",
                        y.Key, y.Value,
                        //y.Value > 3 ? "Hea õpilane" : "Laisk kui lohe"
                        int.TryParse(y.Value, out v) ? v > 3 ? "Hea õpilane" : "Laisk kui lohe" : y.Value
                        );
                }
            }



        }
    }
}
