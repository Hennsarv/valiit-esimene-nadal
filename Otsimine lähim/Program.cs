﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otsimine_lähim
{
    class Program
    {
        static double[,] Arvud = new double[5, 5];
        static Random r = new Random(10);
        static void Main(string[] args)
        {
            Algväärtus();

            //otsimine
            int? rida = null;
            int? veerg = null;
            double vahe = Double.MaxValue;
            Console.Write("Anna arv: ");
            double otsitav = double.Parse(Console.ReadLine());
            for (int i = 0; i < Arvud.GetLength(0); i++)
                for (int j = 0; j < Arvud.GetLength(1); j++)
                {
                    if (vahe > Math.Abs(otsitav - Arvud[i,j]) )
                    {
                        vahe = Math.Abs(otsitav - Arvud[i, j]);
                        rida = i;
                        veerg = j;
                    }
                }
            if (rida.HasValue)
                Console.WriteLine($"Leidsin rida {rida+1} veerg {veerg+1}");
        }

        static void Algväärtus()
        {
            for (int i = 0; i < Arvud.GetLength(0); i++)
                for (int j = 0; j < Arvud.GetLength(1); j++)
                {
                    Arvud[i, j] = r.NextDouble();
                }

            for (int i = 0; i < Arvud.GetLength(0); i++)
            {
                Console.Write($"\nRida {i+1}: ");
                for (int j = 0; j < Arvud.GetLength(1); j++)
                {
                    Console.Write($"{Arvud[i,j]:F5} ");
                }
                
            }
            Console.WriteLine();
        }
    }
}
