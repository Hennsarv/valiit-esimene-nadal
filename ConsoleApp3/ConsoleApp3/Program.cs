﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. trükime välja kõik käsurealt etteantud parameetrid
            // 2. käivitame programmi (käsurealt? kuidas?) koos parameetritega
            // näit c:\> minuprogramm esimene teine kolmas

            // 3. proovige teha sama, aga nii et parameetrid trükitaks ühe reana 
            // komad vahele
            if (args.Length > 0) Console.WriteLine("Tere {0}", args[0]);

            for (int i = 0; i < args.Length; i++)
                Console.WriteLine($"parameeter {i} on {args[i]}");

            StringBuilder sb = new StringBuilder();
            foreach (string x in args)
            {
                if (sb.Length > 0) sb.Append(", ");
                sb.Append(x);
            }
            Console.WriteLine(sb);

            Console.ReadKey();
        }
    }
}
