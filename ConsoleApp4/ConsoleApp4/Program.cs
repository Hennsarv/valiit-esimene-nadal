﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> li = new List<int>();
            List<string> ls;// = new List<string>();

            li.Add(7);
            li.Add(3);
            li.Add(8);
            li.Add(1);
            li.Add(9);

            li[1]++;

            foreach (int x in li) Console.WriteLine(x);
            Console.WriteLine("enne ja pärast");

            li.Sort();

            li.Remove(3);
            foreach (int x in li) Console.WriteLine(x);

            string[] nimed = { "Henn", "Ants", "Peeter" };

            ls = nimed.ToList();
            ls.Sort();
            nimed = ls.ToArray();

            // veel 1 kollektsioon, millest kasu

            Console.WriteLine("dictionary näide\n\n");

            Dictionary<string, int?> Hinded = new Dictionary<string, int?>();
            Hinded.Add("Henn", 5);
            Hinded.Add("Ants", 2);
            Hinded.Add("Peeter", 3);
            Hinded.Add("Juhan", null);


            Console.WriteLine( Hinded["Ants"]   );

            foreach (var x in Hinded)
                Console.WriteLine($"nimi: {x.Key} hinne: {x.Value??0}" );
            float s = 0;
            int mitu = 0;
            foreach (var x in Hinded.Values) if (x.HasValue) { s += x.Value; mitu++; }
            Console.WriteLine("Keskmine hinne on {0}", s / mitu);

            foreach (var x in Hinded.Keys)
                if (Hinded[x].HasValue)
                Console.WriteLine($"Hinde on saanud {x}");


            int? olematu = 7;
            int? olemas = null;
            int ohoo = olematu ?? 0;


        }
    }
}
