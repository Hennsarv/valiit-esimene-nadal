﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiÜlesanne
{
    enum Mast { Risti, Ruutu, Poti, Ärtu}
    enum Tugevus {
        Kaks = 2, Kolm, Neli, Viis, Kuus,
        Seitse, Kaheksa, Üheksa, Kümme,
        Soldat, Emand, Kuningas, Äss
                }
    class Program
    {
        static Kaart[] KaardiPakk;
        static Dictionary<string, Kaart[]> jaotus = new Dictionary<string, Kaart[]>();

        static void Main(string[] args)
        {
            Algseis();


        }

        static void Algseis()
        {
            Console.WriteLine("Avamine");
            KaardiPakk = Kaart.AnnaPakk();
            Kaart.TrükiPakk(KaardiPakk);
            Console.WriteLine("Segamine");
            KaardiPakk = Kaart.SegaPakk(KaardiPakk);
            Kaart.TrükiPakk(KaardiPakk);

            String[] nimed = { "Nora", "Edda", "Susan", "Wendy" };
            int iNimi = 0;
            foreach (var nimi in nimed)
            {
                jaotus.Add(nimi, new Kaart[13]);
                for (int i = 0; i < 13; i++)
                {
                    jaotus[nimi][iNimi % 13] = KaardiPakk[iNimi];
                    iNimi++;
                }
             
            }

            foreach (var x in jaotus)
            {
                Console.WriteLine($"\n{x.Key} kaardid on: ");
                foreach (var y in x.Value)
                    Console.Write( $"{y} " );
            }

        }
    }

    struct Kaart
    {
        static Random r = new Random(10);

        public static Kaart[] AnnaPakk()
        {
            Kaart[] pakk = new Kaart[52];
            for (int i = 0; i < 52; i++)
            {
                pakk[i] = new Kaart((Mast)(i / 13), (Tugevus)(i % 13 + 2));
            }
            return pakk;
        }

        public static Kaart[] SegaPakk(Kaart[] pakk)
        {
            List<Kaart> list = pakk.ToList();
            Kaart[] uusPakk = new Kaart[52];
            for (int i = 0; i < 52; i++)
            {
                int j = r.Next() % list.Count;
                uusPakk[i] = list[j];
                list.Remove(list[j]);
            }
            return uusPakk;
        }

        public static void TrükiPakk(Kaart[] pakk)
        {
            foreach (var x in pakk) Console.WriteLine(x);
            
        }

        public Mast mast;
        public Tugevus tugevus;

        public Kaart (Mast mast, Tugevus tugevus)
        {
            this.mast = mast;
            this.tugevus = tugevus;
        }

        public override string ToString()
        {
            return $"({mast} {tugevus})";
        }
    }
}
