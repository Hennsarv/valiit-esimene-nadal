﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoProjectCS
{
    /// <summary>
    /// meie põhiklass
    /// </summary>
    enum Mast { Risti, Ruutu, Ärtu, Poti }
    enum Värv { Punane, Must }

    class Program
    {

        /// <summary>
        /// Liitmise funktsioon
        /// </summary>
        /// <param name="vasak">vasakpoolne liidetav</param>
        /// <param name="parem">parempoolne liidetav</param>
        /// <returns>nende summa</returns>
        static int Liida(int vasak, int parem)
        {
            return vasak + parem;
        }
        
        
        /// <summary>
        /// see on minu main funktsioon
        /// </summary>
        /// <param name="args">siin on käsurea argumendid</param>
        static void Main(string[] args)
        {

            // see on kommentaar, seda translator ei vaata

 


            Decimal a=0;   // seda hakkame rtuutu tõstma


            Console.Write("Anna üks arv - ma tõstan selle ruutu: ");

            a = Decimal.Parse(Console.ReadLine());
            if (Decimal.TryParse(Console.ReadLine(), out a))
            {
                Console.WriteLine(a * a);
            }
            else
            {
                Console.WriteLine("Ise oled loll");
            }
            //DateTime d = new DateTime();
            //Console.Write("Millal sa sündisid :");
            //d = DateTime.Parse(Console.ReadLine());
            //Console.WriteLine( "oi siis oli {0}", d.DayOfWeek);

            //string s = String.Format("oi siis oli {0}", d.DayOfWeek);

            int i1 = 7; int i2 = 8;
            

            String s = String.Format("{0} * {1} = {2}", i1, i2, i1 * i2);
            Console.WriteLine(s);

            string Nimi = "Mari";
            int Vanus = 29;

            Console.WriteLine( "{0} on {1} aastat vana ({0} on {1} aastane)", Nimi, Vanus);

            // NB! Alates versioonist C# 6
            Console.WriteLine($"{Nimi} on {Vanus} aastat vana");
            Console.WriteLine($"{i1} * {i2} = {i1*i2}");

            String aa = "";
            aa += "Henn";
            aa += " ";
            aa += "Sarv";

            StringBuilder sb = new StringBuilder();
            sb.Append("Henn");
            sb.Append(" ");
            sb.Append("Sarv");
            aa = sb.ToString();

            Console.WriteLine(aa);

            Console.WriteLine("Ütle mast: ");

            Mast m1 = (Mast)(Enum.Parse(typeof(Mast), Console.ReadLine()));

            Värv v1 = (m1 == Mast.Poti || m1 == Mast.Risti) 
                ? Värv.Must 
                : Värv.Punane;

            Console.WriteLine("Ahh sulle meeldib siis {0}", v1);

            switch (m1)
            {
                case Mast.Risti:
                case Mast.Poti:
                    v1 = Värv.Must;
                    break;
                case Mast.Ruutu:
                    Console.WriteLine( "Sul on suht nõrk mast" );
                    goto case Mast.Ärtu;
                case Mast.Ärtu:

                    v1 = Värv.Punane;

                    break;

            }

            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    Console.WriteLine( "Täna mina puhkan" );
                    break;
                case DayOfWeek.Wednesday:
                    Console.WriteLine("Täna õhtul on sulgpall");
                    goto default;
                default:
                    Console.WriteLine("Lähen koju ja vahin telekat");
                    break;
            }





        }
    }

 
}
