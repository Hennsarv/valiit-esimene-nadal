﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{

    class Program
    {
        static void Main(string[] args)
        {

            Inimene i3 = new Inimene("Henn", 62);
            i3 = new Inimene("Peeter", 40);
            i3 = new Inimene("Jaagup", 20);

            Console.WriteLine(Inimene.InimesteArv);

            foreach (var x in Inimene.Inimesed)
                Console.WriteLine(x);
            



        }



        static int Liida(int x, int y, int z)
        {
            return Liida(x, Liida(y, z));
        }
        static int Liida(int x, int y)
        {
            return x + y;
        }

        //static void Tryki(Inimene x)
        //{
        //    Console.WriteLine(x);
        //}
    }

    class Inimene
    {
        public static int InimesteArv = 0;
        public static List<Inimene> Inimesed = new List<Inimene>();

        public static void TrykiInimene(Inimene x)
        {
            Console.WriteLine(x);
        }

        public string Nimi;
        public int Vanus;

        public Inimene(string nimi, int vanus)
        {
            Nimi = nimi; Vanus = vanus;
            InimesteArv++;
            Inimesed.Add(this);
        }

        public Inimene(string nimi) : this(nimi, 0)
        {
            
        }

        public Inimene LisaAastaid(int mitu)
        {
            this.Vanus += mitu;
            return this;
        }

        public Inimene Tryki()
        {
            Console.WriteLine(this);
            return this;
        }

        public override string ToString()
        {
            return $"Inimene {Nimi} vanusega {Vanus}";
        }

        
    }
}
